//controls the Present view
var _markup='',_css='body{background:#000;color:#fff}\n',_head='',_body='';

function slideMarkup(){
collect();
for(i=0;i<slides.length;i++){
_markup+="\n<div id=\'slide"+(1+i)+"\'>\n"+
"<div id=\'slide"+(1+i)+"Title\'>\n"+slides[i]._title+
"</div>\n"+
/*"<div id=\'slide"+_number+"Content\'>\n"+slides[_number]._content+
"</div>\n"+*/

"</div>\n"
;
}
}

function collect(){

for(i=0;i<slides.length;i++){
var _slide=slides[i]._name;
slides[i]._title=sid(_slide+'title').value;
slides[i]._color=sid(_slide+'color').value;
slides[i]._bgColor=sid(_slide+'bgColor').value;
}
}

function slideStyle(){
for(i=0;i<slides.length;i++){
_css+="#slide"+(i+1)+"{background:"+slides[i]._bgColor+"; color:"+slides[i]._color+";}\n"
}
}

function bundleAll(){
slideMarkup();
slideStyle();
var _projectTitle=sid("projectTitle").value || "";
_head=
"<head>\n<meta name='viewport' content='width=device-width,target-densitydpi=device-dpi,initial-scale=1.0,maximum-scale=1.0,user-scalable=0'/>\n"+
"<meta charset='utf-8'>\n"+
"<title>"+_projectTitle+"</title>\n</head>\n";
_body=
"<body>\n<style>\n"+
_css+'\n</style>\n'+
_markup+'\n'+
"</body>\n"
;
_frame=_head+_body;

}
function preview(){
bundleAll();
var page=window.open();
page.document.write("<!Doctype html>\n<html>\n"+_frame+"</html>");
page.document.close();
_markup='';
}

function save(){
bundleAll();
var page=window.open();
page.document.documentElement.innerHTML=("<!Doctype html>\n<html>\n"+_frame+"</html>").replace(/</g,'&lt;').replace(/\n/g,'<br/>');
page.document.close();
_markup='';
}