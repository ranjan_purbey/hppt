//controls the Maker view
function start(){
document.body.style.height=(window.outerHeight)+'px';
window.scrollTo(0,1);
addButton("newProject","Create new project >>",newProject);
sid("newProject").style.fontSize="120%";
}

function newProject(){
var t=0.4;
sid("save").style.display='inline'; sid("preview").style.display='inline-block';
destroy("newProject");

addTextBox("projectTitle","Name your project");
addButton("addSlide","Add Slide",addSlide);

}

function addButton(_id,_text,_onclick,args){
var _span=sid("workZone").appendChild(document.createElement("span"));
_span.innerHTML+="<button id=\'"+_id+"\'>"+_text+"</button>";
sid(_id).onclick=function(){
_onclick(args);
window.scrollTo(0,document.height);
}
}
function addTextBox(_id,_placeholder,_type,_value){
_type=_type || 'text';
var _span=sid("workZone").appendChild(document.createElement("span"));
if(_value!=null)_span.innerHTML+="<input id=\'"+_id+"\' type=\'"+_type+"\' placeholder=\'"+_placeholder+"\' value=\'"+_value+"\'/>";

else _span.innerHTML+="<input id=\'"+_id+"\' type=\'"+_type+"\' placeholder=\'"+_placeholder+"\'/>";

}

function addLabel(_for,_text,_color){
_color=_color || document.body.style.color;
var _span=sid("workZone").appendChild(document.createElement("span"));
_span.innerHTML+="<label for=\'"+_for+"\' style=\'color:"+_color+"\' >"+_text+"</label>";
}

function destroy(srcId){
sid(srcId).parentNode.removeChild(sid(srcId));
}

function br(){
sid("workZone").appendChild(document.createElement("br"));
}
function hr(){
sid("workZone").appendChild(document.createElement("hr"));
}
function addSlide(){
destroy("addSlide");
//till now slides.length is the last slide
slides[slides.length]={'_name':'s'+(slides.length+1)};
//below this slides.length is the current slide, so don't forget to subtract one if you are accessing the elements of the slides array
var _length=slides.length,_slide=slides[_length-1]._name;
hr();
addLabel('','Slide '+_length,"#fad");
br();
addTextBox(_slide+"title","Slide Title");
br();
addLabel(_slide+"color","Font Color");
addTextBox(_slide+"color","Font Color","color","#000000");
br();
addLabel(_slide+"bgColor","Background Color");
addTextBox(_slide+"bgColor","Background Color","color","#ffffff");
br();
addButton('addSlide',"Add another slide",addSlide);
}