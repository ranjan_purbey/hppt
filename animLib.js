function fadeIn(di,t){
var i=0,del=0.05;
if(t==null || t=='')t=1;
var op=window.getComputedStyle(di).opacity;
var x=setInterval(function(){
i++;
var s=window.getComputedStyle(di,null);
if(s.opacity<1)di.style.opacity=op-1+1+del*i;
else if(s.opacity>=1){clearInterval(x);i=0;return true;}
return false;
},t*1000*del)
}

function fadeOut(di,t){
var i=0,del=0.05;
if(t==null || t=='')t=1;
var op=window.getComputedStyle(di).opacity;
var x=setInterval(function(){
i++;
var s=window.getComputedStyle(di,null);
if(s.opacity>0)di.style.opacity=op-1+1-del*i;
if(s.opacity<0.01){clearInterval(x);i=0;return true;}
return false;
},t*1000*del)
}


function fadeTo(di,val,t){
if(t==null || t=='')t=1;
var i=0,del=0.05;
var s=window.getComputedStyle(di,null);
var op=s.opacity;
if(val<s.opacity){var out=setInterval(function(){
i++;
s=window.getComputedStyle(di,null);
if(s.opacity>val)di.style.opacity=op-1+1-del*i;
else{clearInterval(out);i=0;return true};
return false;
},t);
}
else if(val>s.opacity){
var inn=setInterval(function(){
i++;
s=window.getComputedStyle(di,null);
if(s.opacity<val)di.style.opacity=op-1+1+del*i;
else{clearInterval(inn);i=0;return true;}
return false;
},t*1000*del);
}
}

function fadeToggle(di,t){
if(t==null || t=='')t=1;
var s=window.getComputedStyle(di);
if(s.opacity<0.5)return fadeIn(di,t);
else if(s.opacity>=0.5)return fadeOut(di,t);
}

function grow(di,inc,t){
if(t==null || t=='')t=0.4;
if(inc==null || inc=='')inc=0.3;
var i=0,del=0.5;
var s=window.getComputedStyle(di);
var ip=s.fontSize.substring(0,s.fontSize.length-2);
var x=setInterval(function(){

if(del*i<inc*ip)di.style.fontSize=(ip-0+del*i)+"px";
else clearInterval(x);
i++;
},t*1000*del/(ip*inc)
);
}

function shrink(di,inc,t){
if(t==null || t=='')t=0.4;
if(inc==null || inc=='')inc=0.3;
var i=0,del=0.5;
var s=window.getComputedStyle(di);
var ip=s.fontSize.substring(0,s.fontSize.length-2);
var x=setInterval(function(){

if(del*i<=ip*inc/(1+inc))di.style.fontSize=(ip-0-del*i)+"px";
else clearInterval(x);
i++;
},t*1000*del/(ip*inc)
);
}

function sparkle(di,src,t){
if(t==null || t=='')t=1;
var html=src.innerHTML;
var x=setInterval(function(){
di.style.color="rgb("+Math.floor(Math.random()*255)+","+Math.floor(Math.random()*255)+","+Math.floor(Math.random()*255)+")";
},t*200);
src.innerHTML="Stop";
src.onclick=function(){clearInterval(x);src.innerHTML=html;src.onclick=function(){sparkle(di,src);}};
}