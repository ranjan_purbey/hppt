function gradL(ctx,x1,y1,x2,y2,a,colA,b,colB,c,colC,d,colD){
x1=x1 || 0;
y1=y1 || 0;
x2=x2 || 200;
y2=y2 || 10;
a=a || 0;
colA=colA || 'black';
b= b || 1;
colB= colB || 'cyan';
c= c || b;
colC= colC || colB;
d= d || c;
colD= colD || colC;
var grad=ctx.createLinearGradient(x1,y1,x2,y2);
grad.addColorStop(a,colA);
grad.addColorStop(b,colB);
grad.addColorStop(c,colC);
grad.addColorStop(d,colD);
return grad;
}
function gradR(ctx,x1,y1,colA,colB,r1,x2,y2,r2,a,b,c,colC,d,colD){
x2=x2 || x1+2;
y2=y2 || y1+2;
r1=r1 || 1;
r2=r2 || 10;
a=a || 0;
b=b || 1;
c=c || b;
d=d || c;
colA=colA || "#5f9";
colB=colB || "#080";
colC=colC || colB;
colD=colD || colC;
var grad=ctx.createRadialGradient(x1,y1,r1,x2,y2,r2);
grad.addColorStop(a,colA);
grad.addColorStop(b,colB);
grad.addColorStop(c,colC);
grad.addColorStop(d,colD);
return grad;
}
function random(n){
return Math.floor(Math.random()*n);
}
function getCol(r,g,b){
return 'rgb('+r+','+g+','+b+')';
}
function randCol(){
return getCol(random(255),random(255),random(255));
}
//selectors
function sid(id){
return document.getElementById(id);
}
function sclass(cl,i){
i=i || 0;
return document.getElementsByClassName(cl)[i];
}
function stag(tg,i){
i=i || 0;
return document.getElementsByTagName(tg)[i];
}
function toggle(src,fn1,fn2,lbl1,lbl2){
src.onclick=function(){
fn1();
src.innerHTML=lbl2;
src.onclick=function(){
fn2();
src.innerHTML=lbl1;
src.onclick=function(){
fn1();
src.innerHTML=lbl2;
toggle(src,fn2,fn1,lbl2,lbl1);
}
}
}
}
